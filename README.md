# README #

L'objectif de ce projet est de concevoir, réaliser et lancer un grand jeu de sagacité.
Deux exemples sont donnés : https://goo.gl/darj4R.

L’informatique constitue seulement une des facettes du projet. 
Le juridique, le financier, le design on eux aussi été géré.

Sujet : https://drive.google.com/drive/u/0/folders/0B7mNn544KuPGbUNmMXUyMGZhN1E

### Contribution ###

* Sarah Bestaoui
* Alexandre Guedj
* Audouin d'Aboville
* Manon Closset
* Jérôme Leroi

### Version ###
* V 2.0
* Codé en Objective C pour la version mobile
* Codé en HTML/PHP/CSS pour la version web

### Mise à jour ###
* (25/04/2017)
* Refonte du formulaire de réponse aux énigmes
* Synchronisation externe de la base de données
* Optimisation design

### Infos ###

EFREI : Projet Transverse - L3 - 2016