//
//  Historic_View.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 09/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "User_View.h"
#import "Enigme_View.h"

@interface Historic_View : UIViewController
{
    NSString * ID_Send;
}

@property (weak, nonatomic) IBOutlet UITableView *tblPeople;

- (IBAction)Back:(id)sender;


@end
