//
//  BDD_MySQL.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 04/04/2017.
//  Copyright (c) 2017 Audouin d'Aboville. All rights reserved.
//

#ifndef Lutèce_BDD_MySQL_h
#define Lutèce_BDD_MySQL_h

#import <Foundation/Foundation.h>

// -------------------------

@interface Ref : NSObject {
    
    NSString *ID;
    NSString *Titre;
    NSString *Enigme;
    NSString *Reponse;
    NSString *Annecdote;
    NSString *GPS_X;
    NSString *GPS_Y;
    NSString *Arrondissement;
    NSString *Mission;
    NSString *Resolu;
}

@property (nonatomic, retain) NSString *ID;
@property (nonatomic, retain) NSString *Titre;
@property (nonatomic, retain) NSString *Enigme;
@property (nonatomic, retain) NSString *Reponse;
@property (nonatomic, retain) NSString *Annecdote;
@property (nonatomic, retain) NSString *GPS_X;
@property (nonatomic, retain) NSString *GPS_Y;
@property (nonatomic, retain) NSString *Arrondissement;
@property (nonatomic, retain) NSString *Mission;
@property (nonatomic, retain) NSString *Resolu;


@end


#endif
