//
//  User_View.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 09/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "User_View.h"

@interface User_View ()

@end

@implementation User_View

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Affichage du menu user");
}

- (IBAction)Back:(id)sender
{
    ViewController *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:second_view animated:NO completion:nil];
}

- (IBAction)Community:(id)sender
{
    [self showLeaderboardAndAchievements:YES];
   // [self resetAchievements];
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard
{
    GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
    
    gcViewController.gameCenterDelegate = self;
    
    if (shouldShowLeaderboard)
    {
        gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
        gcViewController.leaderboardIdentifier = @"Best_Scores";
    }
    else
    {
        gcViewController.viewState = GKGameCenterViewControllerStateAchievements;
    }
    
    [self presentViewController:gcViewController animated:YES completion:nil];
}

-(void)resetAchievements
{
    [GKAchievement resetAchievementsWithCompletionHandler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}

- (IBAction)Parcours:(id)sender
{
}

- (IBAction)Score:(id)sender
{
    
}

- (IBAction)Historic:(id)sender
{
    
}

@end
