//
//  Enigme_View.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 04/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "ViewController.h"

@interface Enigme_View : UIViewController
{
    NSString * ID;
    NSString * Status_Enigme;
    NSString * Arrondissement;
    NSString * Reponse;
    NSString * Reponse_select;
}

@property (weak, nonatomic) IBOutlet UILabel *Enigme_title;
@property (weak, nonatomic) IBOutlet UILabel *Enigme_annecdote;
@property (weak, nonatomic) IBOutlet UILabel *Enigme_arrondissement;
@property (weak, nonatomic) IBOutlet UIImageView *Enigme_status;
@property (weak, nonatomic) IBOutlet UIImageView *Enigme_arrondissement_pics;
@property (weak, nonatomic) IBOutlet UILabel *Enigme_content;

@property (weak, nonatomic) IBOutlet UILabel *Enigme_reponse;

- (void) setID:(NSString*)my_id;
- (IBAction)Select_answer:(UIButton*)sender;
- (IBAction)Back:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *Button3;
@property (weak, nonatomic) IBOutlet UIButton *Button4;
@property (weak, nonatomic) IBOutlet UIButton *Button1;
@property (weak, nonatomic) IBOutlet UIButton *Button2;



@end
