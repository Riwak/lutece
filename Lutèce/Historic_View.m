//
//  Historic_View.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 09/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "Historic_View.h"

@interface Historic_View ()

@property (nonatomic, strong) DBManager *dbManager;

@property (nonatomic, strong) NSArray *arrPeopleInfo;

-(void)loadData;

@end

@implementation Historic_View

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Affichage de l'historique enigme");
    
    // Make self the delegate and datasource of the table view.
    self.tblPeople.delegate = self;
    self.tblPeople.dataSource = self;
    
    // Initialize the dbManager property.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb.sql"];
    
    // Load the data.
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

-(void)loadData
{
    // Form the query
    NSString *query;
    
    // query = @"select * from Enigme_list where Resolu=0 ORDER BY ID desc";
    query = @"select * from Enigme_list where Resolu=1 ORDER BY ID desc";
    
    // Get the results
    if (self.arrPeopleInfo != nil)
    {
        self.arrPeopleInfo = nil;
    }
    
    self.arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    // Reload the table view
    [self.tblPeople reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrPeopleInfo.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Dequeue the cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"idCellRecord" forIndexPath:indexPath];
        
        NSInteger indexOfFirstname = [self.dbManager.arrColumnNames indexOfObject:@"Titre"];
        // NSInteger indexOfMode = [self.dbManager.arrColumnNames indexOfObject:@"Resolu"];
        NSInteger indexOfAge = [self.dbManager.arrColumnNames indexOfObject:@"Arrondissement"];
        
        // Set the loaded data to the appropriate cell labels.
        cell.textLabel.text = [NSString stringWithFormat:@"%@" , [[self.arrPeopleInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfFirstname]];
        
        cell.detailTextLabel.text = [NSString stringWithFormat:@" Arrondissement : %@", [[self.arrPeopleInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfAge]];
    
    NSLog(@"%@", [self.arrPeopleInfo objectAtIndex:indexPath.row] );
    
    return cell;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    // Get the record ID of the selected name and set it to the recordIDToEdit property.
    ID_Send = [NSString stringWithFormat:@"%@", [[self.arrPeopleInfo objectAtIndex:indexPath.row] objectAtIndex:0]];
    
    // Perform the segue.
    [self performSegueWithIdentifier:@"idSegueEditInfo" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    Enigme_View* secondView =  [self.storyboard instantiateViewControllerWithIdentifier:@"Enigme_View"];
    [secondView setID:ID_Send];
    [self presentViewController:secondView animated:NO completion:nil];
    
}

// LINKERS

- (IBAction)Back:(id)sender
{
    User_View* secondView =  [self.storyboard instantiateViewControllerWithIdentifier:@"User_View"];
    [self presentViewController:secondView animated:NO completion:nil];
}

@end
