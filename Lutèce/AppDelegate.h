//
//  AppDelegate.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 07/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sys/utsname.h>
#import "Start_View.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

