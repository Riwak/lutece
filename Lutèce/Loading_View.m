//
//  Loading_View.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 14/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "Loading_View.h"

@interface Loading_View ()

@property (nonatomic, strong) DBManager *dbManager;

@property (nonatomic, strong) NSArray *arrPeopleInfo;

@end

@implementation Loading_View

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Chargement de la vue loading.");
    
    // Emplacement géographique.
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    NSString *str=[[NSString alloc] initWithFormat:@"latitude:%f longitude:%f",coordinate.latitude,coordinate.longitude];
    NSLog(@"%@",str);
    
    // Recherche de l'arrondissement le plus proche
    // en fonction des coord GPS
    
    double dist=0;
    double distance_mini=1000000000000000000;
    
    // Initialize the dbManager property.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb.sql"];
    
    // Récupération nombre d'enregistrement
    NSString *query = @"select COUNT(*) from Enigme_list";
    NSArray *Total_Columns = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSString *TemporyTotalColumns = [[Total_Columns objectAtIndex:0] objectAtIndex:0];
    int Total = [TemporyTotalColumns intValue];
    
    // Récupération de la base de donnée
    NSString *query2 = @"select * from Enigme_list";
    NSArray *result2 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
    
    // Parcours des résultat
    for (int cpt=0; cpt<=Total-1; cpt++)
    {
        double Enigme_longi = [[[result2 objectAtIndex:cpt] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"GPS_Y"]] doubleValue];
        double Enigme_lati = [[[result2 objectAtIndex:cpt] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"GPS_X"]] doubleValue];
        
        // Calcul de distance
        double Home_longi = Enigme_longi;
        double Home_lati = Enigme_lati;
        double Drone_longi = coordinate.longitude;
        double Drone_lati = coordinate.latitude;
        double rlat1 = 3.141592653589793238462*Home_lati/180;
        double rlat2 = 3.141592653589793238462*Drone_lati/180;
        double theta = Home_longi-Drone_longi;
        double rtheta = 3.141592653589793238462*theta/180;
        double dist = sin(rlat1)*sin(rlat2)+cos(rlat1)*cos(rlat2)*cos(rtheta);
        dist = acos(dist);
        dist = dist*180/3.141592653589793238462;
        dist = dist*60*1.1515;
        dist = dist*1.609344*1000;
        
        //NSLog(@"Recherche de l'arrondissement : %.1f", dist);
        //NSLog(@"Arrondissement : %@", [[result2 objectAtIndex:cpt] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Arrondissement"]]);
        
        if(dist<distance_mini)
        {
            NSLog(@"save!");
            distance_mini=dist;
            Arrondissement_found = [[result2 objectAtIndex:cpt] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Arrondissement"]];
        }
    }
    
    NSLog(@"Arrondissement trouvé : %@ avec dist : %.1f", Arrondissement_found, distance_mini);
    
    // Ecriture arrondissement.
    
    NSString *path = [[self applicationDocumentsDirectory].path
                      stringByAppendingPathComponent:@"arrondissement_select.txt"];
    [Arrondissement_found writeToFile:path atomically:YES
                          encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"arrondissement_select upgrade (bool), %@", Arrondissement_found);
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated
{
    // Processus de synchronisation BDD
    NSString *query;
    NSURL *url = [NSURL URLWithString: @"https://ad-inc.fr/Autres/Lutece/LuteceAPI/API_Service_data.php"];
    
    // Récupération BDD (5 éléments)
    NSLog(@"Lancement de l'API");
    XMLToObjectParser *myParser = [[XMLToObjectParser alloc]
                                   parseXMLAtURL:url toObject:@"Ref" parseError:nil];
    
    for(int i = 0; i < [[myParser items] count]; i++)
    {
        // Vérification de concordance des ID
        // Prendre l'ID BDD en clé
            
        // NSLog(@"id_bdd_(%@)", [(Ref *)[[myParser items] objectAtIndex:i] ID]);
        
        // Inscription en BDD
        query = [NSString stringWithFormat:@"UPDATE Enigme_list SET Titre=\'%@\' WHERE ID=%@",  [[(Ref *)[[myParser items] objectAtIndex:i] Titre] stringByReplacingOccurrencesOfString:@"'" withString:@"`"], [(Ref *)[[myParser items] objectAtIndex:i] ID]];
        [self.dbManager executeQuery:query];
        
        query = [NSString stringWithFormat:@"UPDATE Enigme_list SET Enigme=\'%@\' WHERE ID=%@", [[(Ref *)[[myParser items] objectAtIndex:i] Enigme] stringByReplacingOccurrencesOfString:@"'" withString:@""], [(Ref *)[[myParser items] objectAtIndex:i] ID]];
        [self.dbManager executeQuery:query];
        
        query = [NSString stringWithFormat:@"UPDATE Enigme_list SET Reponse=\'%@\' WHERE ID=%@", [[(Ref *)[[myParser items] objectAtIndex:i] Reponse] stringByReplacingOccurrencesOfString:@"'" withString:@""], [(Ref *)[[myParser items] objectAtIndex:i] ID]];
        [self.dbManager executeQuery:query];

        query = [NSString stringWithFormat:@"UPDATE Enigme_list SET Annecdote=\'%@\' WHERE ID=%@", [[(Ref *)[[myParser items] objectAtIndex:i] Annecdote] stringByReplacingOccurrencesOfString:@"'" withString:@""], [(Ref *)[[myParser items] objectAtIndex:i] ID]];
        [self.dbManager executeQuery:query];

        query = [NSString stringWithFormat:@"UPDATE Enigme_list SET GPS_X=\'%@\' WHERE ID=%@", [[(Ref *)[[myParser items] objectAtIndex:i] GPS_X] stringByReplacingOccurrencesOfString:@"'" withString:@""], [(Ref *)[[myParser items] objectAtIndex:i] ID]];
        [self.dbManager executeQuery:query];

        query = [NSString stringWithFormat:@"UPDATE Enigme_list SET GPS_Y=\'%@\' WHERE ID=%@", [[(Ref *)[[myParser items] objectAtIndex:i] GPS_Y] stringByReplacingOccurrencesOfString:@"'" withString:@""], [(Ref *)[[myParser items] objectAtIndex:i] ID]];
        [self.dbManager executeQuery:query];

        query = [NSString stringWithFormat:@"UPDATE Enigme_list SET Arrondissement=\'%@\' WHERE ID=%@", [[(Ref *)[[myParser items] objectAtIndex:i] Arrondissement] stringByReplacingOccurrencesOfString:@"'" withString:@""], [(Ref *)[[myParser items] objectAtIndex:i] ID]];
        [self.dbManager executeQuery:query];

        query = [NSString stringWithFormat:@"UPDATE Enigme_list SET Mission=\'%@\' WHERE ID=%@", [[(Ref *)[[myParser items] objectAtIndex:i] Mission] stringByReplacingOccurrencesOfString:@"'" withString:@""], [(Ref *)[[myParser items] objectAtIndex:i] ID]];
        [self.dbManager executeQuery:query];
    }
    
    // Fin du processus de récupération
    
    
   ViewController *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
   [self presentViewController:second_view animated:NO completion:nil];
}

@end
