//
//  Score_View.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 09/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface Score_View : UIViewController
{
    
}

@property (weak, nonatomic) IBOutlet UIImageView *Badge1;
@property (weak, nonatomic) IBOutlet UIImageView *Badge2;
@property (weak, nonatomic) IBOutlet UIImageView *Badge3;
@property (weak, nonatomic) IBOutlet UIImageView *Badge4;
@property (weak, nonatomic) IBOutlet UIImageView *Badge5;

@property (weak, nonatomic) IBOutlet UILabel *Pseudo;
@property (weak, nonatomic) IBOutlet UILabel *Level;
@property (weak, nonatomic) IBOutlet UIProgressView *ProgressBar;
@property (weak, nonatomic) IBOutlet UILabel *list_arrondissement;
@property (weak, nonatomic) IBOutlet UILabel *Enigme_tt_resolue;
@property (weak, nonatomic) IBOutlet UILabel *Enigme_tt_restante;
@property (weak, nonatomic) IBOutlet UILabel *Arrondissement_fait;
@property (weak, nonatomic) IBOutlet UILabel *Arrondissement_nonfait;

- (IBAction)Back:(id)sender;


@end
