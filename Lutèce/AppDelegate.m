//
//  AppDelegate.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 07/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    if([machineName() isEqualToString:@"iPhone7,2"] || [machineName() isEqualToString:@"iPhone8,1"])
    {
        NSLog(@"iPhone6");
        
        // Mise en place du fichier syst
        // -----------------------------
        NSString *contenu;
        NSString *chemin = [[self applicationDocumentsDirectory].path
                             stringByAppendingPathComponent:@"syst.txt"];
        
        contenu = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
        
        if ([contenu isEqual:@"0"] || contenu == NULL)
        {
            NSLog(@"Création du fichier init syst");
        }
        
        [@"i6X" writeToFile:chemin atomically:YES
                   encoding:NSUTF8StringEncoding error:nil];
        
        NSLog(@"Machine upgrade (bool), iPhone6");
    }
    
    else if ([machineName() isEqualToString:@"iPhone7,1"] || [machineName() isEqualToString:@"iPhone8,2"])
    {
        NSLog(@"iPhone6 Plus");
        
        // Mise en place du fichier syst
        // -----------------------------
        NSString *contenu;
        NSString *chemin = [[self applicationDocumentsDirectory].path
                            stringByAppendingPathComponent:@"syst.txt"];
        
        contenu = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
        
        if ([contenu isEqual:@"0"] || contenu == NULL)
        {
            NSLog(@"Création du fichier init syst");
        }
        
        [@"i6PX" writeToFile:chemin atomically:YES
                   encoding:NSUTF8StringEncoding error:nil];
        
        NSLog(@"Machine upgrade (bool), iPhone6P");
    }
    
    else if([machineName() isEqualToString:@"iPad6,7"] || [machineName() isEqualToString:@"iPad6,8"] || [machineName() isEqualToString:@"iPad6,3"] || [machineName() isEqualToString:@"iPad6,4"] || [machineName() isEqualToString:@"iPhone5,1"] || [machineName() isEqualToString:@"iPhone5,2"] || [machineName() isEqualToString:@"iPhone5,3"] || [machineName() isEqualToString:@"iPhone5,4"] || [machineName() isEqualToString:@"iPhone6,1"] || [machineName() isEqualToString:@"iPhone6,2"] || [machineName() isEqualToString:@"iPhone8,4"] || [machineName() isEqualToString:@"iPod5,1"] || [machineName() isEqualToString:@"iPod7,1"])
    {
        NSLog(@"iPhone5");
        
        // Mise en place du fichier syst
        // -----------------------------
        NSString *contenu;
        NSString *chemin = [[self applicationDocumentsDirectory].path
                            stringByAppendingPathComponent:@"syst.txt"];
        
        contenu = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
        
        if ([contenu isEqual:@"0"] || contenu == NULL)
        {
            NSLog(@"Création du fichier init syst");
        }
        
        [@"i5X" writeToFile:chemin atomically:YES
                   encoding:NSUTF8StringEncoding error:nil];
        
        NSLog(@"Machine upgrade (bool), iPhone5");
    }
    else
    {
        NSLog(@"iPhone4");
        
        // Mise en place du fichier syst
        // -----------------------------
        NSString *contenu;
        NSString *chemin = [[self applicationDocumentsDirectory].path
                            stringByAppendingPathComponent:@"syst.txt"];
        
        contenu = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
        
        if ([contenu isEqual:@"0"] || contenu == NULL)
        {
            NSLog(@"Création du fichier init syst");
        }
        
        [@"i4X" writeToFile:chemin atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        NSLog(@"Machine upgrade (bool), iPhone4");
    }
    
    return YES;
}

NSString* machineName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
