//
//  Start_View.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 14/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "Start_View.h"

@interface Start_View ()
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation Start_View

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Chargement de la vue start");
    
    // Emplacement géographique.
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    // Vérification de conformité pour les fichiers de sauvegarde
    NSString *contenu;
    NSString *contenu2;
    
    NSString *chemin = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"arrondissement_select.txt"];
    NSString *chemin2 = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"score.txt"];
    
    contenu = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
    contenu2 = [NSString stringWithContentsOfFile:chemin2 encoding:NSUTF8StringEncoding error:nil];
    
    if ([contenu2 isEqual: @"0"] || contenu2 == NULL)
    {
        NSString *path = [[self applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:@"arrondissement_select.txt"];
        NSString *path2 = [[self applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:@"score.txt"];
        
        bool rendu = [@"1" writeToFile:path atomically:YES
                              encoding:NSUTF8StringEncoding error:nil]; // Arrondissement par defaut
        bool rendu2 = [@"10" writeToFile:path2 atomically:YES
                              encoding:NSUTF8StringEncoding error:nil];
        
        NSLog(@"Création des fichiers data : %d, %d", rendu, rendu2);
    }
    
    //  Authentification du joueur auprès de GameCenter
    [self authenticateLocalPlayer];
}

- (void)viewDidAppear:(BOOL)animated
{
    // Mise en place du fichier syst
    // -----------------------------
    NSString *contenu1=@"";
    NSString *chemin1 = [[self applicationDocumentsDirectory].path
                         stringByAppendingPathComponent:@"syst.txt"];
    
    contenu1 = [NSString stringWithContentsOfFile:chemin1 encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"> %@", contenu1);
    
    if ([contenu1 isEqual:@"i6X"] || contenu1 == NULL)
    {
        [@"i6" writeToFile:chemin1 atomically:YES
                  encoding:NSUTF8StringEncoding error:nil];
        
        // Storyboard spécialisé actif : OK!
        NSLog(@"Changement automatique de storyboard - i6");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Start_View"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
    else if ([contenu1 isEqual:@"i6PX"] || contenu1 == NULL)
    {
        [@"i6P" writeToFile:chemin1 atomically:YES
                  encoding:NSUTF8StringEncoding error:nil];
        
        // Storyboard spécialisé actif : OK!
        NSLog(@"Changement automatique de storyboard - i6P");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"iPhone6P" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Start_View"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
    else if ([contenu1 isEqual:@"i5X"] || contenu1 == NULL)
    {
        [@"i5" writeToFile:chemin1 atomically:YES
                  encoding:NSUTF8StringEncoding error:nil];
        
        // Storyboard spécialisé actif : OK!
        NSLog(@"Changement automatique de storyboard - i5");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"iPhone5" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Start_View"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
    else if ([contenu1 isEqual:@"i4X"] || contenu1 == NULL)
    {
        [@"i4" writeToFile:chemin1 atomically:YES
                  encoding:NSUTF8StringEncoding error:nil];
        
        NSString *contenu2;
        NSString *chemin2 = [[self applicationDocumentsDirectory].path
                             stringByAppendingPathComponent:@"syst.txt"];
        contenu2 = [NSString stringWithContentsOfFile:chemin2 encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"> %@", contenu2);
        
        // Storyboard spécialisé actif : OK!
        NSLog(@"Changement automatique de storyboard - i4");
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"iPhone4" bundle:nil];
        UIViewController* initialHelpView = [storyboard instantiateViewControllerWithIdentifier:@"Start_View"];
        initialHelpView.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentModalViewController:initialHelpView animated:YES];
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

-(void)authenticateLocalPlayer
{
    NSLog(@"authenticateLocalPlayer ok");
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil) {
            [self presentViewController:viewController animated:YES completion:nil];
        }
        else{
            if ([GKLocalPlayer localPlayer].authenticated) {
                gameCenterEnabled = YES;
                
                // Get the default leaderboard identifier.
                [[GKLocalPlayer localPlayer] loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString *leaderboardIdentifier, NSError *error) {
                    
                    if (error != nil) {
                        NSLog(@"%@", [error localizedDescription]);
                    }
                    else{
                        leaderboardIdentifier = @"Best_Scores";
                    }
                }];
            }
            
            else{
                gameCenterEnabled = NO;
            }
        }
    };
}

- (IBAction)commencer:(id)sender {
    Loading_View *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"Loading_View"];
    [self presentViewController:second_view animated:NO completion:nil];
}
@end
