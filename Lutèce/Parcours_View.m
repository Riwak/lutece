//
//  Parcours_View.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 09/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "Parcours_View.h"

@interface Parcours_View ()

@property (nonatomic, strong) NSArray *arrPeopleInfo;
-(void)loadData;

@end


@implementation Parcours_View

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Affichage de l'affichage parcours");
    
    // Make self the delegate and datasource of the table view.
    self.tblPeople.delegate = self;
    self.tblPeople.dataSource = self;
    
    [self loadData];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lancez vous dans l'aventure." message:@"Partez à la décourverte d'un arrondissement de votre choix." delegate:self cancelButtonTitle:@"Continuer" otherButtonTitles:nil];
    [alert show];
}

-(void)loadData
{
    // Form the query
    NSString *query;
    
    // query = @"select * from Enigme_list where Resolu=0 ORDER BY ID desc";
    query = @"select * from Enigme_list where Resolu=1 ORDER BY ID desc";
    
    // Get the results
    if (self.arrPeopleInfo != nil)
    {
        self.arrPeopleInfo = nil;
    }
    
    self.arrPeopleInfo = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", nil];
    
    // Reload the table view
    [self.tblPeople reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrPeopleInfo.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Dequeue the cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"idCellRecord" forIndexPath:indexPath];
    
    NSString*txtlabel;
    
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"1"])
    {
        txtlabel=@"I er arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"2"])
    {
        txtlabel=@"II eme arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"3"])
    {
        txtlabel=@"III eme arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"4"])
    {
        txtlabel=@"IV eme arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"5"])
    {
        txtlabel=@"V eme arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"6"])
    {
        txtlabel=@"VI eme arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"7"])
    {
        txtlabel=@"VII eme arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"8"])
    {
        txtlabel=@"VIII eme arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"9"])
    {
        txtlabel=@"IX eme arrondissement";
    }
    if([[self.arrPeopleInfo objectAtIndex:indexPath.row] isEqualToString:@"10"])
    {
        txtlabel=@"X eme arrondissement";
    }
    
    
    cell.textLabel.text = txtlabel;
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Paris - 7500%@", [self.arrPeopleInfo objectAtIndex:indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPat
{
    NSString *path = [[self applicationDocumentsDirectory].path
                      stringByAppendingPathComponent:@"arrondissement_select.txt"];
    
   [[self.arrPeopleInfo objectAtIndex:indexPat.row] writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
   NSLog(@"arrondissement_select upgrade (bool), %@", [self.arrPeopleInfo objectAtIndex:indexPat.row]);
    
    // Perform the segue.
    [self performSegueWithIdentifier:@"idSegueEditInfo" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ViewController* secondView =  [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:secondView animated:NO completion:nil];
    
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (IBAction)Back:(id)sender
{
    
}

@end
