//
//  User_View.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 09/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "GameKit/GameKit.h"

@interface User_View : UIViewController
{
    
}

-(void)resetAchievements;
-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard;

- (IBAction)Back:(id)sender;
- (IBAction)Community:(id)sender;
- (IBAction)Parcours:(id)sender;
- (IBAction)Score:(id)sender;
- (IBAction)Historic:(id)sender;


@end
