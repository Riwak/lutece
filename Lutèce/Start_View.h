//
//  Start_View.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 07/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Loading_View.h"
#import "GameKit/GameKit.h"
#import <CoreLocation/CoreLocation.h>

@interface Start_View : UIViewController
{
    BOOL gameCenterEnabled;
}

- (void)authenticateLocalPlayer;
- (IBAction)commencer:(id)sender;

@end
