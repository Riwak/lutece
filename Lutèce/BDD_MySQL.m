//
//  BDD_MySQL.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 04/04/2017.
//  Copyright (c) 2017 Audouin d'Aboville. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BDD_MySQL.h"

// -------------------------

@implementation Ref

@synthesize ID;
@synthesize Titre;
@synthesize Enigme;
@synthesize Reponse;
@synthesize Annecdote;
@synthesize GPS_X;
@synthesize GPS_Y;
@synthesize Arrondissement;
@synthesize Mission;
@synthesize Resolu;

@end

