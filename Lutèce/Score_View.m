//
//  Score_View.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 09/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "Score_View.h"

@interface Score_View ()
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation Score_View

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Affichage score");
    
    // Affichage levels
    CGAffineTransform transform = CGAffineTransformMakeRotation(0);
    transform = CGAffineTransformScale(transform, 1.0, 5.0);
    [_ProgressBar setTransform:transform];
    
    _Pseudo.text = [[UIDevice currentDevice] name];
    
    // Initialize the dbManager property.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb.sql"];
    
    // BDD Access.
    NSString *query = @"select COUNT(*) from Enigme_list where Resolu=1";
    
    NSArray *Total_Columns = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSString *TemporyTotalColumns = [[Total_Columns objectAtIndex:0] objectAtIndex:0];
    double Total_enigme_resol = [TemporyTotalColumns doubleValue];
    
    _Enigme_tt_resolue.text = TemporyTotalColumns;
    _Enigme_tt_restante.text = [NSString stringWithFormat:@"%.0f", 100-Total_enigme_resol];
    
    double result = ((Total_enigme_resol*2)/10)+1;
    _Level.text = [NSString stringWithFormat:@"%.1f", result];
    
    double calcul = Total_enigme_resol/100;
    _ProgressBar.progress = calcul;
    
    query = @"select COUNT(DISTINCT Arrondissement) from Enigme_list where Arrondissement not in (select Arrondissement from Enigme_list where resolu=0)";
    
    Total_Columns = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    TemporyTotalColumns = [[Total_Columns objectAtIndex:0] objectAtIndex:0];
    double Total_arrondissement_resol = [TemporyTotalColumns doubleValue];
    
    _Arrondissement_fait.text = TemporyTotalColumns;
    _Arrondissement_nonfait.text = [NSString stringWithFormat:@"%.0f", 10-Total_arrondissement_resol];
    
    _list_arrondissement.text = @"N/A";
    
    NSLog(@"Non fait : %f", Total_arrondissement_resol);
    
    if(Total_arrondissement_resol==0)
    {
        NSLog(@"Condition vide");
        _Badge1.image = [UIImage imageNamed:@"arctrimphe_silver.png"];
        _Badge2.image = [UIImage imageNamed:@"cathedrale_silver.png"];
        _Badge3.image = [UIImage imageNamed:@"eiffel_tower_silver.png"];
        _Badge4.image = [UIImage imageNamed:@"invalide_silver.png"];
        _Badge5.image = [UIImage imageNamed:@"louvre_silver.png"];
    }
    else if(Total_arrondissement_resol==1)
    {
         _list_arrondissement.text = [NSString stringWithFormat:@"I"];
        _Badge2.image = [UIImage imageNamed:@"cathedrale_silver.png"];
        _Badge3.image = [UIImage imageNamed:@"eiffel_tower_silver.png"];
        _Badge4.image = [UIImage imageNamed:@"invalide_silver.png"];
        _Badge5.image = [UIImage imageNamed:@"louvre_silver.png"];
    }
    else if (Total_arrondissement_resol==2)
    {
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II"];
        _Badge3.image = [UIImage imageNamed:@"eiffel_tower_silver.png"];
        _Badge4.image = [UIImage imageNamed:@"invalide_silver.png"];
        _Badge5.image = [UIImage imageNamed:@"louvre_silver.png"];
    }
    else if (Total_arrondissement_resol==3)
    {
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II, III"];
        _Badge4.image = [UIImage imageNamed:@"invalide_silver.png"];
        _Badge5.image = [UIImage imageNamed:@"louvre_silver.png"];
    }
    else if (Total_arrondissement_resol==4)
    {
        _Badge5.image = [UIImage imageNamed:@"louvre_silver.png"];
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II, III, IV"];
    }
    else if (Total_arrondissement_resol==5)
    {
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II, III, IV, V"];
    }
    else if (Total_arrondissement_resol==6)
    {
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II, III, IV, V, VI"];
    }
    else if (Total_arrondissement_resol==7)
    {
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II, III, IV, V, VI, VII"];
    }
    else if (Total_arrondissement_resol==8)
    {
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II, III, IV, V, VI, VII, VIII"];
    }
    else if (Total_arrondissement_resol==9)
    {
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II, III, IV, V, VI, VII, VIII, IX"];
    }
    else // 10
    {
        _list_arrondissement.text = [NSString stringWithFormat:@"I, II, III, IV, V, VI, VII, VIII, IX, X"];
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (IBAction)Back:(id)sender
{
    User_View* secondView =  [self.storyboard instantiateViewControllerWithIdentifier:@"User_View"];
    [self presentViewController:secondView animated:NO completion:nil];
}

@end
