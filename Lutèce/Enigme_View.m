//
//  Enigme_View.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 04/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "Enigme_View.h"

@interface Enigme_View ()

@property (nonatomic, strong) DBManager *dbManager;

@property (nonatomic, strong) NSArray *arrPeopleInfo;

-(void)loadData;

@end

@implementation Enigme_View

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Affichage de la vue enigme");
    NSLog(@"GET_ID : %@", ID);
    
    Reponse_select = @"";

    // Initialize the dbManager property.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb.sql"];
    
    // Load the data.
    [self loadData];
}

-(void) viewWillAppear:(BOOL)animated
{
    
}

-(void) setID:(NSString*)my_id
{
    ID=my_id;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)loadData
{
    NSLog(@"Load Data");
    
    // Create the query.
    NSString *query = [NSString stringWithFormat:@"select * from Enigme_list where ID=%@", ID];
    NSLog(@"%@", query);
    
    // Load the relevant data.
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    // Set the loaded data to the view
    
    _Enigme_title.text = [[results objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Titre"]];
    Status_Enigme = [[results objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Resolu"]];
    _Enigme_content.text = [[results objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Enigme"]];
    Arrondissement = [[results objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Arrondissement"]];
    Reponse = [[results objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Reponse"]];
    _Enigme_reponse.text = Reponse;
    
    _Enigme_arrondissement.text = [Arrondissement stringByAppendingString:@" arrondissement de Paris"];
    
    // Icônes d'arrondissement.
    if([Arrondissement isEqual: @"1"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"cathedrale.png"];
    }
    else if ([Arrondissement isEqual: @"2"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"invalide.png"];
    }
    else if ([Arrondissement isEqual: @"3"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"louvre.png"];
    }
    else if ([Arrondissement isEqual: @"4"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"pyramide.png"];
    }
    else if ([Arrondissement isEqual: @"5"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"pompidou.png"];
    }
    else if ([Arrondissement isEqual: @"6"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"eiffel_tower.png"];
    }
    else if ([Arrondissement isEqual: @"7"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"arctrimphe.png"];
    }
    else if ([Arrondissement isEqual: @"8"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"basilique.png"];
    }
    else if ([Arrondissement isEqual: @"9"])
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"communauté.png"];
    }
    else // 10
    {
        _Enigme_arrondissement_pics.image = [UIImage imageNamed:@"paris_metro.png"];
    }
    
    // Génération du QCM.
    // -----------------------------
    NSString *id_false1, *id_false2, *id_false3;
    
    // 1ère Fausse Réponse
    NSString *wronganswer_request = [NSString stringWithFormat:@"SELECT * FROM Enigme_list WHERE ID!=%@ ORDER BY RANDOM() LIMIT 1 ", ID];
    NSArray *results_wrong_task = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:wronganswer_request]];
    
    id_false1 = [[results_wrong_task objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"ID"]];
    NSString *content1 = [[results_wrong_task objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Reponse"]];
    
    // 2nd Fausse Réponse
    wronganswer_request = [NSString stringWithFormat:@"SELECT * FROM Enigme_list WHERE ID!=%@ AND ID!=%@ ORDER BY RANDOM() LIMIT 1 ", ID, id_false1];
    results_wrong_task = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:wronganswer_request]];
    
    id_false2 = [[results_wrong_task objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"ID"]];
    NSString *content2 = [[results_wrong_task objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Reponse"]];
    
    // 3ème Fausse Réponse
    wronganswer_request = [NSString stringWithFormat:@"SELECT * FROM Enigme_list WHERE ID!=%@ AND ID!=%@ AND ID!=%@ ORDER BY RANDOM() LIMIT 1 ", ID, id_false1, id_false2];
    results_wrong_task = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:wronganswer_request]];
    
    id_false3 = [[results_wrong_task objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"ID"]];
    NSString *content3 = [[results_wrong_task objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Reponse"]];
    
    // Choix du bouton réponse
    int WichButton = (((float)arc4random()/0x100000000)*(4-0)+1);
    NSLog(@"SelectButton : %d", WichButton);
    
    NSLog(@"%@", content1);
    NSLog(@"%@", content2);
    NSLog(@"%@", content3);
    
    if (WichButton == 1)
    {
        [_Button1 setTitle:Reponse forState:UIControlStateNormal];
        [_Button2 setTitle:content1 forState:UIControlStateNormal];
        [_Button3 setTitle:content2 forState:UIControlStateNormal];
        [_Button4 setTitle:content3 forState:UIControlStateNormal];

    }
    else if (WichButton == 2)
    {
        [_Button1 setTitle:content1 forState:UIControlStateNormal];
        [_Button2 setTitle:Reponse forState:UIControlStateNormal];
        [_Button3 setTitle:content2 forState:UIControlStateNormal];
        [_Button4 setTitle:content3 forState:UIControlStateNormal];
    }
    else if (WichButton == 3)
    {
        [_Button1 setTitle:content1 forState:UIControlStateNormal];
        [_Button2 setTitle:content2 forState:UIControlStateNormal];
        [_Button3 setTitle:Reponse forState:UIControlStateNormal];
        [_Button4 setTitle:content3 forState:UIControlStateNormal];
    }
    else // WichButton == 4
    {
        [_Button1 setTitle:content1 forState:UIControlStateNormal];
        [_Button2 setTitle:content2 forState:UIControlStateNormal];
        [_Button3 setTitle:content3 forState:UIControlStateNormal];
        [_Button4 setTitle:Reponse forState:UIControlStateNormal];
    }
    
    
    
    if([Status_Enigme isEqual:@"0"])
    {
        _Button1.hidden=NO;
        _Button2.hidden=NO;
        _Button3.hidden=NO;
        _Button4.hidden=NO;
        
        _Enigme_status.image = [UIImage imageNamed:@"un_done.png"]; // Image pas répondu
        _Enigme_annecdote.text = @""; // Pas d'annecdote
        _Enigme_reponse.hidden=YES; // Pas de réponse
        
        NSString *chemin = [[self applicationDocumentsDirectory].path
                            stringByAppendingPathComponent:@"tempory_block.txt"];
        NSString *contenu_file = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
        
        bool rendu = [@"" writeToFile:chemin atomically:YES
                             encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"Update fichier de blocage (UNLOCK) : %d", rendu);
    }
    else
    {
        _Enigme_reponse.hidden=NO; // Affichage de la réponse
        _Enigme_status.image = [UIImage imageNamed:@"done.png"]; // Image répondu
        _Enigme_annecdote.text = [[results objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Annecdote"]]; // Anecdote
        
        _Button1.hidden=YES; // Masquage du champs de réponse
        _Button2.hidden=YES;
        _Button3.hidden=YES;
        _Button4.hidden=YES;
    }
    
    NSLog(@"Enigme réponse : '%@'", Reponse);
}

// Focus Enigme
// ------------

- (IBAction)Select_answer:(UIButton*)sender
{
    Reponse_select = sender.currentTitle;
    
    NSLog(@"Vous avez choisi : '%@'",Reponse_select);
    
    if ([Reponse_select isEqual: Reponse])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bien joué ! " message:@"Réponse correcte." delegate:self cancelButtonTitle:@"Continuer" otherButtonTitles:nil];
        [alert show];
        
        // Inscription en BDD
        NSString *query = [NSString stringWithFormat:@"UPDATE Enigme_list SET Resolu=1 WHERE ID=%@", ID];
        [self.dbManager executeQuery:query];
        [self loadData];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Dommage ! " message:@"Votre réponse est incorrecte. Cette enigme est maintenant verrouillé :'( \n Resolvez une autre énigme afin de débloquer celle-ci!" delegate:self cancelButtonTitle:@"Continuer" otherButtonTitles:nil];
        [alert show];
        
        // Verrouillage de l'enigme car echec
        NSString *chemin = [[self applicationDocumentsDirectory].path
                            stringByAppendingPathComponent:@"tempory_block.txt"];
        bool rendu = [ID writeToFile:chemin atomically:YES
                                  encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"Update fichier de blocage (LOCK) : %d", rendu);
        
        // Back to view Controller
        ViewController* secondView =  [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self presentViewController:secondView animated:NO completion:nil];
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (IBAction)Back:(id)sender
{
    ViewController* secondView =  [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:secondView animated:NO completion:nil];
}

@end
