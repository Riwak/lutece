//
//  XMLToObjectParser.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 04/04/2017.
//  Copyright (c) 2017 Audouin d'Aboville. All rights reserved.
//

//XMLToObjectParser.h
#import <Foundation/Foundation.h>

@interface XMLToObjectParser : NSObject <NSXMLParserDelegate> {
    NSString *className;
    NSMutableArray *items;
    NSObject *item; // stands for any class
    NSString *currentNodeName;
    NSMutableString *currentNodeContent;
}
- (NSArray *)items;
- (id)parseXMLAtURL:(NSURL *)url
           toObject:(NSString *)aClassName
         parseError:(NSError **)error;
@end
