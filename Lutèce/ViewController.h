//
//  ViewController.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 07/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sys/utsname.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "User_View.h"
#import "DBManager.h"
#import "Enigme_View.h"
#import <Social/Social.h>


@interface ViewController : UIViewController
{
    CLLocationCoordinate2D MyCoord;
    double level;
}

@property (weak, nonatomic) IBOutlet UIWebView *WebAnalytics;
@property (weak, nonatomic) IBOutlet UILabel *Paris;
@property (weak, nonatomic) IBOutlet UIImageView *EiffelTower;
@property (weak, nonatomic) IBOutlet UILabel *Level;
@property (weak, nonatomic) IBOutlet UILabel *Arrondissement;
@property (weak, nonatomic) IBOutlet UILabel *RatioEnigme;
@property (weak, nonatomic) IBOutlet UILabel *RatioEnigme2;
@property (weak, nonatomic) IBOutlet UIProgressView *ProgressEnigme;
@property (weak, nonatomic) IBOutlet UILabel *MessageBox;

@property (weak, nonatomic) IBOutlet MKMapView *MapView;


- (IBAction)UserOrientation:(id)sender;
- (void) LoadData;
- (IBAction)Change_Map_Type:(id)sender;
- (IBAction)FocusMap:(id)sender;
- (IBAction)ShareButton:(id)sender;
- (IBAction)UserButton:(id)sender;

@end

