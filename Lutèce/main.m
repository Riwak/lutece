//
//  main.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 07/04/2016.
//  Copyright © 2016 Audouin d'Aboville, Adrien Herbert, Alexandre Guedj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
