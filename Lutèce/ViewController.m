//
//  ViewController.m
//  Lutèce
//
//  Created by Audouin d'Aboville on 07/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import "ViewController.h"

#define METERS_PER_MILE 1609.344

@interface ViewController ()
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, strong) DBManager *dbManager;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Chargement de la vue ViewController");
    
    //  Espace publicitaire & google analytics
    [_WebAnalytics loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString : @"http://ad-inc.fr/Autres/Lutece/game.html"]]];
    NSLog(@"Analytics init ok");
    
    // Initialisation BDD
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb.sql"];
    
    // Affichage levels
    CGAffineTransform transform = CGAffineTransformMakeRotation(0);
    transform = CGAffineTransformScale(transform, 1.0, 5.0);
    [_ProgressEnigme setTransform:transform];
    
    // Bloque la mise en veille automatique
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    
    // Emplacement géographique.
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    // Instanciation de la carte
    locationManager.delegate=self;
    locationManager = [[CLLocationManager alloc]init];
    [locationManager requestWhenInUseAuthorization];
    CLLocationManager *locationManager2 = [[CLLocationManager alloc] init];
    locationManager2.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager2.distanceFilter = kCLDistanceFilterNone;
    [locationManager2 startUpdatingLocation];
    CLLocation *location2 = [locationManager2 location];
    CLLocationCoordinate2D coordinate2 = [location2 coordinate];
    CLLocationCoordinate2D test_Coord = coordinate2;
    MyCoord = test_Coord;
    
    [_MapView setCenterCoordinate:test_Coord];
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(test_Coord, 1.2*METERS_PER_MILE, 1.2*METERS_PER_MILE);
    [_MapView setRegion:viewRegion animated:NO];
    
    
    // Récupération du fichier arrondissement
    
    NSString *chemin = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"arrondissement_select.txt"];
    NSString *chemin2 = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"score.txt"];
    _Arrondissement.text = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
    NSString *tempory_score_value = [NSString stringWithContentsOfFile:chemin2 encoding:NSUTF8StringEncoding error:nil];
    
    // Initialize the dbManager property.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb.sql"];
    
    // BDD Access.
    NSString *query = @"select COUNT(*) from Enigme_list where Resolu=1 AND Arrondissement=";
    query = [query stringByAppendingString:_Arrondissement.text];
    NSArray *Total_Columns = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSString *TemporyTotalColumns = [[Total_Columns objectAtIndex:0] objectAtIndex:0];
    double Total_enigme_resol_arrondissement = [TemporyTotalColumns doubleValue];
    
    NSString *query9 = @"select COUNT(*) from Enigme_list where Arrondissement=";
    query9 = [query9 stringByAppendingString:_Arrondissement.text];
    NSArray *Total_Columns9 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query9]];
    NSString *TemporyTotalColumns9 = [[Total_Columns9 objectAtIndex:0] objectAtIndex:0];
    double Total_enigme_unresolv_arrondissement = [TemporyTotalColumns9 doubleValue];

    _RatioEnigme.text = TemporyTotalColumns;
    _RatioEnigme.text = [_RatioEnigme.text stringByAppendingString:@"/"];
    _RatioEnigme.text = [_RatioEnigme.text stringByAppendingString:TemporyTotalColumns9];
    
    double calcul = (Total_enigme_resol_arrondissement/Total_enigme_unresolv_arrondissement);
    
    _ProgressEnigme.progress = calcul;
    NSLog(@"Progress : %f, %f", _ProgressEnigme.progress, Total_enigme_resol_arrondissement);
    
    // -------------------------
    
    query = @"select COUNT(*) from Enigme_list where Resolu=1";
    Total_Columns = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    TemporyTotalColumns = [[Total_Columns objectAtIndex:0] objectAtIndex:0];
    double Total_enigme_resolues = [TemporyTotalColumns doubleValue];
    
    double result = ((Total_enigme_resolues*2)/10)+1;
    _Level.text = [@"Level : " stringByAppendingString:[NSString stringWithFormat:@"%.1f", result]];
    level=result*10;
    
    _MessageBox.text = TemporyTotalColumns9;
    _MessageBox.text = [_MessageBox.text stringByAppendingString:@" énigmes disponible dans le "];
    _MessageBox.text = [_MessageBox.text stringByAppendingString:_Arrondissement.text];
    _MessageBox.text = [_MessageBox.text stringByAppendingString:@"ème arrondissement."];
    
    // Load the data.
    [self LoadData];
    
    if ([tempory_score_value doubleValue]<level)
    {
        // Envois des données sur Game-Center
        NSLog(@"Update Score");
        [self reportScore];
        
        NSString *path2 = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"score.txt"];
        
        [[NSString stringWithFormat:@"%.f", level] writeToFile:path2 atomically:YES
                              encoding:NSUTF8StringEncoding error:nil];
    }
}

- (IBAction)UserOrientation:(id)sender
{
    [_MapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
}

-(void) LoadData
{
    CLLocationCoordinate2D Location;
    
    // Récupération nombre d'enregistrement
    NSString *query2 = @"select COUNT(*) from Enigme_list where Arrondissement=";
    query2 = [query2 stringByAppendingString:_Arrondissement.text];
    NSArray *Total_Columns = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
    NSString *TemporyTotalColumns = [[Total_Columns objectAtIndex:0] objectAtIndex:0];
    int Total = [TemporyTotalColumns intValue];
    
    // Travail sur la base de données
    NSString *query = @"select * from Enigme_list where Arrondissement=";
    query = [query stringByAppendingString:_Arrondissement.text];
    NSArray *result = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    self.MapView.delegate = self;
    
    // Parcours des résultat
    for (int cpt=0; cpt<=Total-1; cpt++)
    {
        NSLog(@"Ajout annotation %d/%d", cpt, Total-1);
        // Ajout de une annotation avec verrou
        Location.latitude=[@([[[result objectAtIndex:cpt] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"GPS_X"]] doubleValue]) doubleValue];
        Location.longitude=[@([[[result objectAtIndex:cpt] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"GPS_Y"]] doubleValue]) doubleValue];
        
        NSLog(@"X : %f", Location.latitude);
        NSLog(@"Y : %f", Location.longitude);
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = Location;
        annotation.subtitle = [[result objectAtIndex:cpt] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"ID"]];
        annotation.title = [[result objectAtIndex:cpt] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Titre"]];
        
        [_MapView addAnnotation: annotation];
    }
}

#pragma mark Delegate Methods

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
    {
    id <MKAnnotation> annotation = [view annotation];
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        NSLog(@"Clicked : %@", annotation.subtitle);
        
        CLLocationManager *locationManager2 = [[CLLocationManager alloc] init];
        locationManager2.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager2.distanceFilter = kCLDistanceFilterNone;
        [locationManager2 startUpdatingLocation];
        CLLocation *location2 = [locationManager2 location];
        CLLocationCoordinate2D coordinate2 = [location2 coordinate];
        
        // Vérification de proximité
        // Calcul de distance
        double Home_longi = annotation.coordinate.longitude;
        double Home_lati = annotation.coordinate.latitude;
        double Drone_longi = coordinate2.longitude;
        double Drone_lati = coordinate2.latitude;
        double rlat1 = 3.141592653589793238462*Home_lati/180;
        double rlat2 = 3.141592653589793238462*Drone_lati/180;
        double theta = Home_longi-Drone_longi;
        double rtheta = 3.141592653589793238462*theta/180;
        double dist = sin(rlat1)*sin(rlat2)+cos(rlat1)*cos(rlat2)*cos(rtheta);
        dist = acos(dist);
        dist = dist*180/3.141592653589793238462;
        dist = dist*60*1.1515;
        dist = dist*1.609344*1000;
        
        // L'énigme doit être à moins de 150 m
        if(dist<150)
        {
            NSString *chemin = [[self applicationDocumentsDirectory].path
                                stringByAppendingPathComponent:@"tempory_block.txt"];
            NSString *contenu_file = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
            
            if ([contenu_file isEqualToString:annotation.subtitle])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enigme verrouillé !" message:@"Cette enigme est verrouillé. Pour la débloquer veuillez résoudre une autre énigme. Bonne chance ;) !!" delegate:self cancelButtonTitle:@"Continuer" otherButtonTitles:nil];
                [alert show];
            }
            else
            {
                Enigme_View* secondView =  [self.storyboard instantiateViewControllerWithIdentifier:@"Enigme_View"];
                [secondView setID:annotation.subtitle];
                [self presentViewController:secondView animated:NO completion:nil];
            }
        }
        else
        {
            NSString *query2 = [NSString stringWithFormat:@"select * from Enigme_list where ID=%@",annotation.subtitle];
            NSArray *result2 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
            
            if ([[[result2 objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Resolu"]] isEqualToString:@"1"])
            {
                Enigme_View* secondView =  [self.storyboard instantiateViewControllerWithIdentifier:@"Enigme_View"];
                [secondView setID:annotation.subtitle];
                [self presentViewController:secondView animated:NO completion:nil];
            }
            else
            {
                NSString*variabletxt = [NSString stringWithFormat:@"Vous êtes trop loin de cette énigme pour y accéder (il vous reste %.1f mètres à faire).",dist-140];
            
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rapprochez vous !" message:variabletxt delegate:self cancelButtonTitle:@"Continuer" otherButtonTitles:nil];
                [alert show];
            }
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
           pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
           pinView.canShowCallout = YES;
            
            NSString *query2 = [NSString stringWithFormat:@"select * from Enigme_list where ID=%@",annotation.subtitle];
            NSLog(@"%@", query2);
            NSArray *result2 = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
           
            if ([[[result2 objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Resolu"]] isEqualToString:@"1"])
            {
               pinView.image = [UIImage imageNamed:@"green_pin.png"];
            }
            else
            {
                pinView.image = [UIImage imageNamed:@"red_pin.png"];
            }
            
            pinView.calloutOffset = CGPointMake(0, 32);
            
            // Add a detail disclosure button to the callout.
            UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            pinView.rightCalloutAccessoryView = rightButton;
            
            UIImageView *iconView;
            
            // Add an image to the left callout.
            if ([[[result2 objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"Resolu"]] isEqualToString:@"1"])
            {
                 iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"green_pin.png"]];
            }
            else
            {
                 iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red_pin.png"]];
            }
            
            pinView.leftCalloutAccessoryView = iconView;
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

-(void) viewWillAppear:(BOOL)animated
{

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reportScore
{
    double level_sent = level;
    NSLog(@"Reporting Score to GameCenter : %f", level_sent);
    
    GKScore *score = [[GKScore alloc] initWithLeaderboardIdentifier:@"Best_Scores"];
    score.value = level_sent;
    
    [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
        else{
            NSLog(@"No problem reported");
        }
    }];
}

- (IBAction)Change_Map_Type:(id)sender
{
    switch (((UISegmentedControl *) sender).selectedSegmentIndex)
    {
        case 0:
            [_MapView setMapType:MKMapTypeStandard];
            _Level.textColor=[UIColor blueColor];
            _Arrondissement.textColor=[UIColor blueColor];
            _RatioEnigme.textColor=[UIColor blueColor];
            _RatioEnigme2 .textColor=[UIColor blueColor];
            _Paris.textColor=[UIColor blueColor];
            _EiffelTower.image=[UIImage imageNamed:@"king.png"];
            break;
        case 1:
            [_MapView setMapType:MKMapTypeSatelliteFlyover];
            _Level.textColor=[UIColor whiteColor];
            _Arrondissement.textColor=[UIColor whiteColor];
            _RatioEnigme.textColor=[UIColor whiteColor];
            _RatioEnigme2.textColor=[UIColor whiteColor];
            _Paris.textColor=[UIColor whiteColor];
            _EiffelTower.image=[UIImage imageNamed:@"king_white.png"];
            break;
    }
}

- (IBAction)FocusMap:(id)sender
{
    // Envois des données sur Game-Center
    //[self reportScore];
    
    NSLog(@"Focus");
    ViewController *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:second_view animated:NO completion:nil];
}

- (IBAction)ShareButton:(id)sender
{
    NSLog(@"Facebook");
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController * fbSheetOBJ = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString  *Var_temp1 = _Level.text;
        
        NSString *msg = [NSString stringWithFormat:@"J'ai atteint le level %@ sur Lutèce ! Etês vous capable de faire mieux ?", Var_temp1];
        
        [fbSheetOBJ setInitialText:msg];
        [fbSheetOBJ addURL:[NSURL URLWithString:@"https://www.facebook.com/LuteceParrot/"]];
        [fbSheetOBJ addImage:[UIImage imageNamed:@"parrot2.png"]];
        
        [self presentViewController:fbSheetOBJ animated:YES completion:Nil];
    }
}

- (IBAction)UserButton:(id)sender
{
    User_View *second_view = [self.storyboard instantiateViewControllerWithIdentifier:@"User_View"];
    [self presentViewController:second_view animated:NO completion:nil];
}

@end
