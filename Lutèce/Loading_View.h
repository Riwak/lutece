//
//  Loading_View.h
//  Lutèce
//
//  Created by Audouin d'Aboville on 14/04/2017.
//  Copyright © 2017 Audouin d'Aboville, Adrien Herbert, Jérôme Leroi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>
#import "ViewController.h"

#import "DBManager.h"
#import "BDD_MySQL.h"
#import "XMLToObjectParser.h"

@interface Loading_View : UIViewController
{
    NSString* Arrondissement_found;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;


@end
