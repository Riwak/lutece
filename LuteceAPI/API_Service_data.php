<?php
	// API de récupération BDD
	
	try {	
			$cpt++;		
   			$dbh = new PDO("sqlite:sampledb.sql");
    		$sql = "SELECT * FROM Enigme_list";
    		
    		$xml = new SimpleXMLElement('<xml/>');
    		$track = $xml->addChild('Ref');
    		 
    		foreach ($dbh->query($sql) as $row)
        	{
        		$cpt++;
        		$track->addChild('ID', "$row[0]");
   				$track->addChild('Titre', "$row[1]");
   				$track->addChild('Enigme', "$row[2]");
   				$track->addChild('Reponse', "$row[3]");
   				$track->addChild('Annecdote', "$row[4]");
   				$track->addChild('GPS_X', "$row[5]");
   				$track->addChild('GPS_Y', "$row[6]");
   				$track->addChild('Arrondissement', "$row[7]");
   				$track->addChild('Mission', "$row[8]"); // Synchro attribute
   				$track->addChild('Resolu', "$row[9]");
            	
            	$track = $xml->addChild('Ref');

				/*** close the database connection ***/
    			$dbh = null;
    		}
		}
		catch(PDOException $e)
    	{
    		echo $e->getMessage();
    	}	
    
    	
	Header('Content-type: text/xml');
	print($xml->asXML());
	
?>